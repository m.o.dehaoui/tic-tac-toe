<?php
include "./connection.php";
header("Content-Type: application/json");
$data = json_decode(stripslashes(file_get_contents("php://input")));
$winner = json_decode($data->winner);
$data = $data->list ;
if(count($data)>0 ){
	if($winner == 0 || $winner == 1 || $winner == 2){
		if($winner == 0)
			$winner = null;
		$stmt = $conn->prepare("INSERT INTO GAME (id,winner) VALUES (:id,:winner)");
		$stmt->bindValue(':id', null, PDO::PARAM_NULL);
		$stmt->bindValue(':winner', $winner);
		$stmt->execute();
		$stmt = $conn->prepare("SELECT ID FROM GAME ORDER BY ID DESC LIMIT 1"); 
		$stmt->execute(); 
		$game_id = $stmt->fetchColumn();
		$stmt = $conn->prepare("INSERT INTO MOVE (game_id,player_id,square) VALUES (:game_id,:player_id,:square)");

		foreach ($data as $key => $value) {


			$player = $value->player;
			$square = $value->square ;

			if($player == 1 || $player == 2){
				if( $square >0 && $square <=9){
					$stmt->bindParam(':game_id', $game_id,PDO::PARAM_INT);
					$stmt->bindParam(':player_id', $player,PDO::PARAM_INT);
					$stmt->bindParam(':square', $square,PDO::PARAM_INT);
					$stmt->execute();
					echo json_encode($square);
				}
			
			}
		}
	}
}
?>


