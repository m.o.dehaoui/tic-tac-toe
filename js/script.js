
	var solutions = [
		{box1:1, box2:2,box3:3},
		{box1:4, box2:5,box3:6},
		{box1:7, box2:8,box3:9},
		{box1:1, box2:5,box3:9},
		{box1:1, box2:4,box3:7},
		{box1:2, box2:5,box3:8},
		{box1:3, box2:6,box3:9},
		{box1:3, box2:5,box3:7}]; // All the possible solutions to win 
			
			var squareValue=0;
			var player2 = document.getElementById("player2");
			var player1 = document.getElementById("player1");
			player1.innerHTML = "your turn";
			var won = false;
			var moves = [];
		
		// check if the square is empty and give it the right value depending on whos player turn is.
		function giveValue(id){
		if(document.getElementById(id).innerHTML == "" && won == false ){
				if(squareValue % 2 == 0){
				document.getElementById(id).innerHTML = "x";
				player2.innerHTML = "your turn";
				player1.innerHTML = "";
				moves.push({"square" : id ,"player" : 1});
				}
				else{
				document.getElementById(id).innerHTML = "o";
				player2.innerHTML = "";
				player1.innerHTML = "your turn";
				moves.push({"square" : id ,"player" : 2});
				}
				squareValue++;
			if(squareValue >= 5)
				checkWinner();
			if(squareValue == 9 && won == false){
				alert("call it even");
				player2.innerHTML = "loser";
				player1.innerHTML = "loser";
				result = { winner : 0 , list : moves}
				send_value(result);
			}
		}
		}
		
		// the role of this function is to check the already defined list "solutions" 
		// and see if there is any solution of them exist
		// and calling the function send_value to send  list of moves when we have a winner
		// means in case x  or o positions match one of the solutions.
		function checkWinner(){
			solutions.forEach(function(element){
				if(document.getElementById(element.box1).innerHTML == "x" && document.getElementById(element.box2).innerHTML =="x"
				 && document.getElementById(element.box3).innerHTML == "x"){
					 won = true;
				player2.innerHTML = "loser";
				player1.innerHTML = "winner";
				player1.style.color = "blue";
				result = { winner : 1 , list : moves}
				send_value(result);
				}
				else if(document.getElementById(element.box1).innerHTML == "o" && document.getElementById(element.box2).innerHTML =="o"
				 && document.getElementById(element.box3).innerHTML =="o") {
					 won = true;
				player2.innerHTML = "winner";
				player2.style.color = "blue";
				player1.innerHTML = "loser";
				result = { winner : 2 , list : moves}
				send_value(result);
				}	
			});
		}
		//return to position 0, by calling this function we rest the game to the initial state
		function replay(){
			moves = [];
			squareValue=0;
			player1.innerHTML = "your turn";
			player2.innerHTML = "";
			player1.style.color = "red";
			player1.style.color = "red";
			won = false;
			for(var i=0;i<9 ; i++){
				document.getElementsByClassName("changingButton")[i].innerHTML = "";
			}
		}
		//Ajax to send the moves in json format
		function send_value(vars) {
		var xmlhttp = new XMLHttpRequest();   
		xmlhttp.open("POST", "api/index.php", true);
		xmlhttp.setRequestHeader("Content-type", "application/json;charset=UTF-8");
		var jsonArray = JSON.stringify( vars);
		xmlhttp.send(jsonArray); 
		}