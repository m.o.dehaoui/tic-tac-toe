# Tic-Tac-Toe Data  Assembler

Tic-tac-toe (also known as noughts and crosses or Xs and Os) is a well-known paper-and-pencil game for two players, X and O, who take turns marking the spaces in a 3×3 grid. The player who succeeds in placing three of their marks in a horizontal, vertical, or diagonal row wins the game.
this is a web-based application which assembles data to train a machine learning algorithm in order to always win a  game of tic-tac-toe. 

### Prerequisites

in order to run the game, you need a browser 
```
google chrome , Mozilla Firefox ..
```
you will need a PHP development environment and a local server XAMPP or WampServer will be perfect.

### basic thoughts about how i solved the assignment

first thing I did is writing an algorithm that solves the game problem, then I tried to applicate that algorithm with javascript,
after that, all I needed is to find the right places to save the moves, the winner of the game and send data in JSON format using ajax, then I developed the PHP part to save the data in MySQL database.

### notice

* Use the tictactoe.sql file to create the database,the database name need to be "tictactoe" and having 2 players with the id 1 and 2 is required.
* If there is a draw the field winner in the game table will have a null value.
* In bigger projects using a NoSQL database like MongoDB will be a better choice, but in the small project like this one MySQL will do just fine.
* In bigger projects using other blanches on git, like "develop", "hotfixes" and keeping the master blanch just for deliverable versions will be the right thing to do.


